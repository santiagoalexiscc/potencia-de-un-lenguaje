/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package potencia.de.un.lenguaje;
import java.util.Arrays;
import java.util.Scanner;
import java.util.Set;
/**
 *
 * @author PC
 */
public class PotenciaDeUnLenguaje {

    
    public static String[] generarPotencia(String a, int n)  {
             if (n == 0) {
            String[] conjuntoVacio = {"λ"};
            return conjuntoVacio; // El conjunto vacío es la potencia cero del lenguaje
        } else if (n == 1) {
            String[] lenguaje = new String[a.length()];
            for (int i = 0; i < a.length(); i++) {
                lenguaje[i] = Character.toString(a.charAt(i));
            }
            return lenguaje; // La potencia uno es el propio lenguaje
        } else {
            String[] potenciaN_1 = generarPotencia(a, n - 1); // Obtenemos la potencia n-1
            String[] nuevaPotencia = new String[potenciaN_1.length * a.length()]; // Creamos un nuevo arreglo vacío para la potencia n
            int k = 0; // Contador para el nuevo arreglo de la potencia n
            for (int i = 0; i < potenciaN_1.length; i++) {
                for (int j = 0; j < a.length(); j++) {
                    String nuevaCadena = potenciaN_1[i] + Character.toString(a.charAt(j)); // Concatenamos la cadena actual con cada símbolo de a
                    nuevaPotencia[k++] = nuevaCadena; // Añadimos la nueva cadena al arreglo de la potencia n, con un salto de línea al final
                }
            }
            return nuevaPotencia;
        }
    }
     public static void main(String[] args) {
          String a = "ab";
        String[] conjuntoVacio = {"λ"};
        Scanner sc = new Scanner(System.in);
        System.out.println("Ingrese la cantidad de n para la cerraduta de Kleene.");
        int n = sc.nextInt();
        if (n == 0) {
            String[] potencia = generarPotencia(a, n);
            System.out.println("Potencia de " + a + " elevado a " + n + ": " + Arrays.toString(potencia));

        } else {
            String[] potencia = generarPotencia(a, n);
            System.out.println("Potencia de " + a + " elevado a " + n + ": " + Arrays.toString(conjuntoVacio) + Arrays.toString(potencia));
        }
    }
    
}
